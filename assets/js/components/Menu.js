'use strict';

import Events from './Events';

export default class Menu {
	constructor() {
		const maxWidth = 992;

		$('.js-burger').click(function () {
			let time = 50;
			if ($(this).hasClass('burger--open')) {

				$('.js-animation').removeClass('animated fadeInLeft');
				$(this).removeClass('burger--open');
				$(this).closest('.left').removeClass('left--open');
				
				Events.enableScroll();
			} else {

				$('.js-animation').each((i, el) => {
					setTimeout(() => {
						$(el).addClass('animated fadeInLeft');
					}, time);
					time += 50;
				});
				$(this).addClass('burger--open');
				$(this).closest('.left').addClass('left--open');

				Events.disableScroll(maxWidth);
			}
			
			$('.js-menu').toggleClass('menu--open');
		});

		$('.js-left-hover').click(function() {
			$('.js-animation').removeClass('animated fadeInLeft');
			$('.js-burger').removeClass('burger--open');
			$('.js-burger').closest('.left').removeClass('left--open');
			$(document).off('scroll touchmove mousewheel');
			$ ('.js-menu').toggleClass ('menu--open');
		});
	}

}
