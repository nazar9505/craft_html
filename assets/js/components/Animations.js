'use strict';

import { TweenMax } from 'gsap';
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

export default class Animations {
	constructor () {
		let tooSmall = false;
		this.controller = null;
		this.maxWidth = 992;

		if ($ (window).width () < this.maxWidth) {
			tooSmall = true;
		}

		if (!tooSmall) {
			this.initScrollMagic ();
		}

		this.resizeHandler();
	}
	initScrollMagic() {
		$('body').scrollTop(0);
		this.controller = new ScrollMagic.Controller({ vertical: true });

		const self = this;
		$('.js-animation-scrolling').each(function(index, element) {
			const scene = new ScrollMagic.Scene({
				triggerElement: this,
				duration: $(this).height() * 1.5,
				triggerHook: 1,
			})
				.setTween(
					TweenMax.fromTo(
						this,
						1,
						{ y: $(this).data('offset') },
						{ y: 0, ease: Linear.easeInOut }
					)
				)
				.addTo(self.controller);
		});
	}
	resizeHandler() {
		const self = this;
		$(window).resize(function() {
			const wWidth = $(window).width();
			if (wWidth < this.maxWidth) {
				if (self.controller) {
					self.controller = self.controller.destroy(true);
				}
			} else if (wWidth >= this.maxWidth) {
				if (!self.controller) {
					self.initScrollMagic();
				}
			}
		});
	}
}
