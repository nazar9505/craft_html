'use strict';

import Events from './Events';

export default class Form{
    static openModal(target) {
        $(target).addClass('opened');
        $('body').addClass('opened-popup');
        $('.js-hover').addClass('opened');
    }
    static closeModal(target) {
        target.removeClass('opened');
        $('.js-hover').removeClass('opened');
        $('body').removeClass('opened-popup');
    }
    static initModals() {
        const self = this;
        $('.js-modal-open').click(function (e) {
            e.preventDefault();
            var target = $(this).data('target');
            self.openModal(target);
        });

        $('.js-modal-close').click(function () {
            self.closeModal($(this).closest('.modal'));
        });

        $('.js-hover').click(function () {
            self.closeModal($('.js-modal.opened'));
        });

        $('.js-modal').click(function (e) {
            if ($(e.target).hasClass('js-modal')) {
                self.closeModal($(e.target));
            }
        });
    }
}