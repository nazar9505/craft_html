'use strict';

export default class Events{
    static enableScroll(){
        $ (document).off ('scroll touchmove mousewheel');
    }
    static disableScroll(maxWidth = 0){
        $(document).on(
            'scroll touchmove mousewheel',
            function (e) {
                if ($(window).width() > maxWidth) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            }
        );
    }
}