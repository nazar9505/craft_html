// Uncomment if necessary
// let fullpage = require('./vendor/jquery.fullpage.min');
// let masked = require('./vendor/jquery.maskedinput.min');

import './vendor/owl.carousel.min';
import './vendor/lightbox.min';

window.Form = Form;
window.$ = $;

// let scrolloverflow = require('./vendor/scrolloverflow.min');

import Carousel from './components/Carousel';
import Menu from './components/Menu';
import Animations from './components/Animations';
import Form from './components/Form';
import Promo from './components/Promo';

$(() => {

	new Carousel();
	new Menu();
	new Animations();
	new Promo();

	Form.initModals();
		
});