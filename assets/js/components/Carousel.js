"use strict";

export default class Carousel {
  constructor() {
    var th = this;
    $(".owl-carousel").each(function(i, el) {
      var $this = $(this);
      var items = parseFloat($this.data("items"));
      var nav = $this.data("nav");
      var dots = $this.data("dots");
      var loop = $this.data("loop");
      var responsive = options($this.data("responsive"));
      var prev = $this.data("prev");
      var next = $this.data("next");
      var autoplay = $this.data("autoplay");

      $this.owlCarousel({
        items: items,
        nav: nav,
        dots: dots,
        autoHeight: true,
        slideBy: 2,
        loop: loop,
        scrollPerPage: true,
        responsive: responsive,
        navText: [prev, next],
        mouseDrag: false,
        autoplay: autoplay
      });
    });
    function options(string) {
      if (typeof string != "string") return string;

      if (string.indexOf(":") != -1 && string.trim().substr(-1) != "}") {
        string = "{" + string + "}";
      }

      var start = string ? string.indexOf("{") : -1,
        options = {};

      if (start != -1) {
        try {
          options = str2json(string.substr(start));
        } catch (e) {}
      }
      return options;
    }
    function str2json(str, notevil) {
      try {
        if (notevil) {
          return JSON.parse(
            str
              .replace(/([\$\w]+)\s*:/g, function(_, $1) {
                return '"' + $1 + '":';
              })
              .replace(/'([^']+)'/g, function(_, $1) {
                return '"' + $1 + '"';
              })
          );
        } else {
          return new Function(
            "",
            "var json = " + str + "; return JSON.parse(JSON.stringify(json));"
          )();
        }
      } catch (e) {
        return false;
      }
    }
  }
}
